package top.fullj.money;

import java.math.BigDecimal;
import java.util.Currency;

/**
 * @author bruce.wu
 * @since 2021/11/12 11:08
 */
public interface ExchangeRateProvider {

    Conversion getConversion(String foreignCurrency);

    BigDecimal query(String localCurrency, String foreignCurrency);

    BigDecimal query(Currency localCurrency, Currency foreignCurrency);

}
