package top.fullj.money;

import java.math.BigDecimal;
import java.util.Currency;

/**
 * @author bruce.wu
 * @since 2021/11/15 10:22
 */
public abstract class ExchangeRateProviderAdapter
        implements ExchangeRateProvider {

    @Override
    public Conversion getConversion(String foreignCurrency) {
        return new DefaultConversion(Currency.getInstance(foreignCurrency), this);
    }

    @Override
    public BigDecimal query(Currency localCurrency, Currency foreignCurrency) {
        return query(localCurrency.getCurrencyCode(), foreignCurrency.getCurrencyCode());
    }

}
