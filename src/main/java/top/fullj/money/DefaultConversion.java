package top.fullj.money;

import javax.annotation.Nonnull;
import java.math.BigDecimal;
import java.util.Currency;

/**
 * @author bruce.wu
 * @since 2021/11/15 14:57
 */
class DefaultConversion implements Conversion {

    private final Currency foreignCurrency;

    private final ExchangeRateProvider provider;

    DefaultConversion(Currency currency, ExchangeRateProvider provider) {
        this.foreignCurrency = currency;
        this.provider = provider;
    }

    @Nonnull
    @Override
    public Money apply(@Nonnull Money money) {
        BigDecimal exchangeRate = provider.query(money.currency(), foreignCurrency);
        BigDecimal amount = money.amount().multiply(exchangeRate);
        return Money.of(amount, foreignCurrency);
    }

}
