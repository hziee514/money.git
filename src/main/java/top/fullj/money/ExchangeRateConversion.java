package top.fullj.money;

import javax.annotation.Nonnull;
import java.math.BigDecimal;
import java.util.Currency;

/**
 * @author bruce.wu
 * @since 2021/11/15 15:06
 */
public class ExchangeRateConversion implements Conversion {

    private final Currency foreignCurrency;

    private final BigDecimal exchangeRate;

    public ExchangeRateConversion(String currency, BigDecimal exchangeRate) {
        this(Currency.getInstance(currency), exchangeRate);
    }

    public ExchangeRateConversion(Currency currency, BigDecimal exchangeRate) {
        this.foreignCurrency = currency;
        this.exchangeRate = exchangeRate;
    }

    @Nonnull
    @Override
    public Money apply(@Nonnull Money money) {
        BigDecimal amount = money.amount().multiply(exchangeRate);
        return Money.of(amount, foreignCurrency);
    }

}
