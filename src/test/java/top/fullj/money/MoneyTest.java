package top.fullj.money;

import org.junit.Test;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Currency;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;

/**
 * @author bruce.wu
 * @since 2021/11/12 10:02
 */
public class MoneyTest {

    @Test
    public void newMoneyWithBigDecimal() {
        Money money = Money.of("1.2345", "CNY");
        assertEquals(new BigDecimal("1.23"), money.amount());
    }

    @Test
    public void calc() {
        Money money = Money.parse("CNY0.01");
        assertEquals(Money.parse("CNY0.05"), money.add(Money.parse("CNY0.04")));
        assertEquals(Money.parse("CNY0.05"), money.add(new BigDecimal("0.04")));
        money = Money.parse("CNY0.10");
        assertEquals(Money.parse("CNY0.05"), money.sub(Money.parse("CNY0.05")));
        assertEquals(Money.parse("CNY0.05"), money.sub(new BigDecimal("0.05")));
        money = Money.parse("CNY0.10");
        assertEquals(Money.parse("CNY0.01"), money.mul(0.1));
        assertEquals(Money.parse("CNY0.50"), money.mul(5));
        assertEquals(Money.parse("CNY0.02"), money.mul(new BigDecimal("0.2")));
        assertEquals(Money.parse("CNY1.02"), money.mul(new BigDecimal("10.2")));
    }

    @Test
    public void allocEqualDivision() {
        Money money = Money.of("1.23", Currency.getInstance("CNY"));
        System.out.println(money);
        Money[] div = money.alloc(4);
        System.out.println(Arrays.toString(div));
        assertEquals(money, money.alloc(1)[0]);
    }

    @Test
    public void allocByRatios() {
        long[] allocation = {3,7};
        Money money = Money.of("0.05", Currency.getInstance("CNY"));
        Money[] div = money.alloc(allocation);
        System.out.println(Arrays.toString(div));

        div = Money.of("1.23", Currency.getInstance("CNY"))
                .alloc(new long[] {1,2,3,4});
        System.out.println(Arrays.toString(div));

        div = Money.of("1.23", Currency.getInstance("CNY"))
                .alloc(new long[] {1,1,1,1});
        System.out.println(Arrays.toString(div));

        assertEquals(money, money.alloc(new long[]{50})[0]);
    }

    @Test
    public void formatAndParse() {
        Money money = Money.parse("CNY1.23");
        String str = money.toString();
        Money parsed = Money.parse(str);
        assertEquals(money, parsed);
        System.out.println(str);
        System.out.println(parsed);
    }

    @Test
    public void with() {
        Money money = Money.of("1.23", Currency.getInstance("CNY"));
        Money touch = money.with(new BigDecimal("1.23"));
        System.out.println(money);
        System.out.println(touch);
        assertEquals(money, touch);
        assertNotSame(money, touch);
    }

    @Test
    public void defaultCurrencyCodeNotConfiguredWithStr() {
        Money.setDefaultCurrencyCode(null);
        Money money = Money.of("1.23");
        System.out.println(money);
        assertEquals("CNY1.23", money.toString());
    }

    @Test
    public void defaultCurrencyCodeNotConfiguredWithZero() {
        Money.setDefaultCurrencyCode(null);
        Money money = Money.zero();
        System.out.println(money);
        assertEquals("CNY0.00", money.toString());
    }

    @Test
    public void defaultCurrencyCodeNotConfiguredWithBigDecimal() {
        Money.setDefaultCurrencyCode(null);
        Money money = Money.of(BigDecimal.valueOf(1.23D));
        System.out.println(money);
        assertEquals("CNY1.23", money.toString());
    }

    @Test
    public void defaultCurrencyCodeConfiguredWithStr() {
        Money.setDefaultCurrencyCode("USD");
        Money money = Money.of("1.23");
        System.out.println(money);
        assertEquals("USD1.23", money.toString());
    }

    @Test
    public void defaultCurrencyCodeConfiguredWithZero() {
        Money.setDefaultCurrencyCode("USD");
        Money money = Money.zero();
        System.out.println(money);
        assertEquals("USD0.00", money.toString());
    }

    @Test
    public void defaultCurrencyCodeConfiguredWithBigDecimal() {
        Money.setDefaultCurrencyCode("USD");
        Money money = Money.of(BigDecimal.valueOf(1.23D));
        System.out.println(money);
        assertEquals("USD1.23", money.toString());
    }

}